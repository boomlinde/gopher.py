#! /usr/bin/env python

from sys import argv
import os
import json
from twisted.internet import protocol, reactor
from twisted.protocols.basic import LineOnlyReceiver

def read_in_chunks(file_object):
	while True:
		data = file_object.read(1024)
		if not data:
			break
		yield data

class GopherHandler():
    def __init__(self, location, config):
        self.location = os.path.expanduser(location)
        if not self.location.endswith(os.sep):
            self.location += os.sep
        self.HOST = str(config.get('hostname', '127.0.0.1'))
        self.PORT = config.get('ext_port', config.get('port', 8086))
        self.types = config.get('types', {})

    def request(self, source, address):
        if '..' in address:
            source.transport.loseConnection()
            return
        address = os.sep.join(address.split('/'))
        loc = self.location + address
        if os.path.isdir(loc):
            self.dir_list(source, address)
            return
        elif os.path.isfile(loc):
            for data in read_in_chunks(file(loc)):
                source.transport.write(data)
        source.transport.loseConnection()

    def dir_list(self, source, address):
        try:
            out = [x.rstrip() for x in file(os.path.join(self.location + address, '.head')).readlines()]
        except Exception:
            out = ['i%s' % self.HOST, 'iListing: %s' % address, 'i']
        try:
            exclude = [x.rstrip() for x in file(os.path.join(self.location + address, '.exclude')).readlines()]
        except Exception:
            exclude = []

        out = [x + '\tfake\t(NULL)\t0' for x in out if x.startswith('i')]

        for entry in os.listdir(self.location + address):
            if entry.startswith('.') or entry in exclude:
                continue
            try:
                type = entry.rsplit('.', 1)[1].lower()
            except Exception:
                type = None
            if os.path.isdir(os.path.join(self.location + address, entry)):
                type = 1
            else:
                type = self.types.get(type, 9)
            link = '%s%s\t%s\t%s\t%d' % (str(type), entry, '/' + os.path.join(address, entry), self.HOST, self.PORT)
            out.append(link)
        out.append('.')
        source.sendLine('\r\n'.join(out))
        source.transport.loseConnection()

config = json.loads(file(os.path.join(argv[1], '.config')).read())
handler = GopherHandler(argv[1], config)

class Gopher(LineOnlyReceiver):
    def lineReceived(self, line):
        handler.request(self, line)

    def connectionLost(self, reason):
        self.transport.loseConnection()

class GopherFactory(protocol.Factory):
    def buildProtocol(self, addr):
        return Gopher()

reactor.listenTCP(config.get('port', 8086), GopherFactory())
reactor.run()

